import os

import locapip

import asyncio
import json
import omni.ext
import omni.kit.asset_converter
import omni.kit.stage_templates
import omni.kit.stage_templates
import omni.kit.ui
import omni.timeline
import omni.ui
import omni.usd
import shutil
from omni.kit.capture import CaptureExtension, CaptureRangeType, CaptureRenderPreset, CaptureStatus
from pathlib import Path
from pxr import Gf, Usd, UsdGeom, UsdShade, UsdPhysics, PhysxSchema, Sdf


class Extension(omni.ext.IExt):
    def on_startup(self, ext_id):
        locapip.module['fit_fill'].new_stage = new_stage

        locapip.module['fit_fill'].mesh_reset['DynamicBody'] = mesh_reset('DynamicBody')
        locapip.module['fit_fill'].mesh_build['DynamicBody'] = mesh_build('DynamicBody')

        locapip.module['fit_fill'].mesh_reset['StaticBody'] = mesh_reset('StaticBody')
        locapip.module['fit_fill'].mesh_build['StaticBody'] = mesh_build('StaticBody')

        locapip.module['fit_fill'].simulate = simulate

    def on_shutdown(self):
        pass


transform = {}


def new_stage(message):
    # 新建默认场景
    omni.kit.stage_templates.new_stage(template='sunlight')
    stage: Usd.Stage = omni.usd.get_context().get_stage()

    # 设置时间坐标
    stage.SetTimeCodesPerSecond(24)
    stage.SetStartTimeCode(0)
    stage.SetEndTimeCode(144)

    # 设置朝上方向
    UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)

    # 设置长度单位cm
    UsdGeom.SetStageMetersPerUnit(stage, 0.01)

    # 创建物理场景
    path: Sdf.Path = stage.GetDefaultPrim().GetPath().AppendPath('PhysicsScene')
    scene = UsdPhysics.Scene.Define(stage, path)
    scene.CreateGravityDirectionAttr(Gf.Vec3d(0.0, 0.0, -1.0))
    scene.CreateGravityMagnitudeAttr(981.0 * 0.1)  # 减小重力以降低错误率

    # 创建物理材质
    path: Sdf.Path = stage.GetDefaultPrim().GetPath().AppendPath('PhysicsMaterial')
    material = UsdShade.Material.Define(stage, path)
    mat_api = UsdPhysics.MaterialAPI.Apply(material.GetPrim())
    mat_api.CreateStaticFrictionAttr(0.01)
    mat_api.CreateDynamicFrictionAttr(0.01)
    mat_api.CreateRestitutionAttr(0.5)
    mat_api.CreateDensityAttr(1000.0)

    global transform
    transform = {}

    return json.dumps({})


def mesh_reset(name: str):
    async def foo(server_path: str):
        stage: Usd.Stage = omni.usd.get_context().get_stage()
        path: Sdf.Path = stage.GetDefaultPrim().GetPath().AppendPath(name)
        stage.RemovePrim(path)
        mesh = UsdGeom.Mesh.Define(stage, path)

        # 转换选项
        context = omni.kit.asset_converter.AssetConverterContext()
        context.ignore_materials = False  # 导出材质
        context.ignore_animations = False  # 导出动画
        context.ignore_camera = False  # 导出相机
        context.ignore_light = False  # 导出光照
        context.single_mesh = True  # 导出单一文件，而不是一个网格一个文件
        context.smooth_normals = True  # 生成光滑法线
        context.export_preview_surface = False  # 生成预览表面
        context.embed_mdl_in_usd = True  # 嵌入MDL材质，不生成材质文件
        context.use_meter_as_world_unit = False  # 使用m作为世界单位，默认是cm
        context.create_world_as_default_root_prim = False  # 创建/World作为根元素
        context.embed_textures = False  # 嵌入纹理，适用于导出fbx
        context.create_folder = False  # 创建独立文件夹
        context.folder_name = None  # 独立文件夹名称

        # 导入网格体
        output_path = server_path[:-4] + '.usd'
        converter = omni.kit.asset_converter.get_instance()
        task = converter.create_converter_task(server_path, output_path, None, context)
        success = await task.wait_until_finished()
        if not success:
            detailed_status_code = task.get_status()
            detailed_status_error_string = task.get_detailed_error()
            raise Exception(f'{detailed_status_code}: {detailed_status_error_string}')

        point_num = 0
        face_num = 0

        # 拷贝数据
        temp_stage = Usd.Stage.Open(output_path)
        for prim in temp_stage.Traverse():
            if prim.GetTypeName() == 'Mesh':
                temp_mesh = UsdGeom.Mesh(prim)
                mesh.CreatePointsAttr(temp_mesh.GetPointsAttr().Get())
                mesh.CreateExtentAttr(temp_mesh.GetExtentAttr().Get())
                mesh.CreateNormalsAttr(temp_mesh.GetNormalsAttr().Get())
                mesh.CreateFaceVertexCountsAttr(temp_mesh.GetFaceVertexCountsAttr().Get())
                mesh.CreateFaceVertexIndicesAttr(temp_mesh.GetFaceVertexIndicesAttr().Get())

                point_num = len(mesh.GetPointsAttr().Get())
                face_num = len(mesh.GetFaceVertexCountsAttr().Get())
                break

        return point_num, face_num

    return foo


def mesh_build(name):
    def foo():
        stage: Usd.Stage = omni.usd.get_context().get_stage()
        path: Sdf.Path = stage.GetDefaultPrim().GetPath().AppendPath(name)
        mesh: UsdGeom.Mesh = UsdGeom.Mesh.Define(stage, path)

        path = stage.GetDefaultPrim().GetPath().AppendPath('PhysicsMaterial')
        material = UsdShade.Material.Define(stage, path)

        api: UsdShade.MaterialBindingAPI = UsdShade.MaterialBindingAPI.Apply(mesh.GetPrim())
        api.Bind(material, UsdShade.Tokens.weakerThanDescendants, 'physics')
        UsdPhysics.CollisionAPI.Apply(mesh.GetPrim())

        if name in ['DynamicBody']:
            rb_api = UsdPhysics.RigidBodyAPI.Apply(mesh.GetPrim())
            api = UsdPhysics.MeshCollisionAPI.Apply(mesh.GetPrim())
            api.CreateApproximationAttr('convexDecomposition')

            api = PhysxSchema.PhysxConvexDecompositionCollisionAPI.Apply(mesh.GetPrim())
            api.CreateMaxConvexHullsAttr(32)  # 凸包的最大数量
            api.CreateHullVertexLimitAttr(60)  # 凸包包含顶点的的最大数量
            api.CreateMinThicknessAttr(0.1)  # 凸包最小厚度
            api.CreateVoxelResolutionAttr(5e5)  # 体素分辨率
            api.CreateErrorPercentageAttr(10)  # 体素误差百分比(0.01~25)
            api.CreateShrinkWrapAttr(True)  # 投影凸包顶点到网格体表面以逼近曲面形状

    return foo


async def simulate(message):
    global transform
    request = json.loads(message)
    capture_path = request['capture_path']
    output_folder = str(Path(capture_path).parent)

    stage: Usd.Stage = omni.usd.get_context().get_stage()
    timeline = omni.timeline.get_timeline_interface()

    capture = CaptureExtension.get_instance()
    capture.options.camera = '/OmniverseKit_Persp'  # 捕获相机
    capture.options.range_type = CaptureRangeType.FRAMES  # 捕获范围方式
    capture.options.capture_every_nth_frames = -1  # 每n帧捕获一帧
    capture.options.fps = int(timeline.get_time_codes_per_seconds())  # 每秒帧数
    capture.options.start_frame = int(stage.GetStartTimeCode())  # 按帧捕获的开始帧
    capture.options.end_frame = int(stage.GetEndTimeCode())  # 按帧捕获的结束帧
    capture.options.start_time = timeline.get_start_time()  # 按秒捕获的开始时刻
    capture.options.end_time = timeline.get_end_time()  # 按秒捕获的结束时刻
    capture.options.res_width = 1920  # 横分辨率
    capture.options.res_height = 1080  # 纵分辨率
    capture.options.render_preset = CaptureRenderPreset.PATH_TRACE  # 捕获渲染方式
    capture.options.output_folder = output_folder  # 输出目录
    capture.options.file_name = capture_path  # 文件名
    capture.options.file_name_num_pattern = '.####'  # 文件名编号格式
    capture.options.file_type = '.mp4'  # 文件类型
    capture.options.save_alpha = False  # 保存alpha通道

    def progress_update_fn(capture_status, *args):
        if capture_status == CaptureStatus.FINISHING:
            shutil.copyfile(str(Path(capture_path + '.mp4')), str(Path(capture_path).parent) + '.mp4')
            shutil.rmtree(Path(capture_path).parent)
            path = stage.GetDefaultPrim().GetPath().AppendPath('DynamicBody')
            mesh: UsdGeom.Mesh = UsdGeom.Mesh.Define(stage, path)
            transform['DynamicBody']: Gf.Matrix4d = mesh.GetLocalTransformation()

    transform = {}
    capture.progress_update_fn = progress_update_fn
    capture.start()

    async def result():
        while 'DynamicBody' not in transform:
            await asyncio.sleep(0.1)

        m = {'vectors': [{'values': [0, 0, 0, 0]},
                         {'values': [0, 0, 0, 0]},
                         {'values': [0, 0, 0, 0]},
                         {'values': [0, 0, 0, 0]}]}

        for r in range(4):
            for c in range(4):
                m['vectors'][r]['values'][c] = transform['DynamicBody'][r][c]

        return json.dumps({'matrix': m})

    return await result()
