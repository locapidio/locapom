# Fit and Fill Simulation

Simulate the collision and matching process between meshes, and output the transformation matrix in a stable state

模拟网格体之间碰撞匹配的过程，输出稳定状态的变换矩阵

## 更新日志

### 0.1.7

* pip install locapip==0.1.17
* 名称由locapom.fit.fill改为locapom.fit_fill
* 移除了启动参数
* 移除了流传输网格体，改为从explorer路径读取网格体

### 0.1.6

* pip install locapip==0.1.15
* 重写了客户端，整合了大部分低级API
* 现在每个接口都可以多次调用，可多次上传重定义网格体，可多次模拟

### 0.1.5

* pip install locapip==0.1.14
* 分离了服务locapom.server作为独立的扩展

### 0.1.4

* pip install locapip==0.1.14
* 使用async/await改写了模拟rpc，避免了轮询

### 0.1.3

* pip install locapip==0.1.13
* 增加了rpc new_stage用于场景初始化，以确保在上传模型之前正确应用场景参数

## 服务端

### 安装

参照 [locapom.server](https://gitlab.com/locapidio/locapom/tree/master/exts/locapom.server)

### 配置项

| 配置 | 类型 | 描述 |
| ------ | ------ | ------ |
| ```exts/locapom.server/port``` | ```int``` | 服务端口 |
| ```exts/locapom.explorer/working_directory``` | ```str``` | 服务的工作目录，是所有数据的根目录 |

### 启动

确保```ov/cache/gitexts```路径下的历史缓存已删除

通过```ov/pkg/create-2021.x.x```路径下的```omni.create.bat```命令启动服务

```bat
call "ov/pkg/create-2021.x.x/omni.create.bat" ^
--ext-folder git+https://gitlab.com/locapidio/locapom?branch=master&dir=exts ^
--/exts/locapom.server/port=6547 ^
--/exts/locapom.explorer/working_directory=D:/ue ^
--enable locapom.fit_fill
```

## 客户端

### 准备python环境

进入python3.7~3.9环境，安装locapip，因为用到vtk读取stl文件，所以还要安装vtk

```bat
cmd
python -m pip install -U locapip vtk
```

### 示例

准备网格体文件，运行以下代码

```python
import locapip
from vtkmodules.vtkIOGeometry import vtkSTLReader, vtkSTLWriter

import locale
import tempfile
from pathlib import Path

# 导入模块
locapip.reload_module()

# 服务器地址
url = 'localhost:6547'

# 初始化场景
locapip.module['fit_fill'].new_stage(url, {})

# 上传网格体，使用vtkPolyData
reader = vtkSTLReader()
locale.setlocale(locale.LC_ALL, 'zh_CN.UTF-8')
reader.SetFileName('d:/ue/FitFillFemur.stl')
reader.Update(None)

with tempfile.TemporaryDirectory() as tmpdir:
    path = str(Path(tmpdir) / '.stl')
    locale.setlocale(locale.LC_ALL, 'zh_CN.UTF-8')
    w = vtkSTLWriter()
    w.SetInputData(reader.GetOutput())
    w.SetFileName(path)
    w.Write()

    server_path = '.cache/fit_fill/FitFillFemur.stl'

    locapip.module['explorer'].copy(url, {
        'copy_type': 'upload',
        'client_path': path,
        'server_path': server_path
    })
    locapip.module['fit_fill'].define_static_body(url, {
        'server_path': server_path
    })

# 上传网格体，使用STL文件路径
dynamic_body_paths = ['d:/ue/FitFillStem.stl', 'd:/ue/FitFillStem.stl', 'd:/ue/FitFillStem.stl']

for path in dynamic_body_paths:
    server_path = str(Path('.cache/fit_fill') / Path(path).name)

    locapip.module['explorer'].copy(url, {
        'copy_type': 'upload',
        'client_path': path,
        'server_path': server_path
    })
    locapip.module['fit_fill'].define_dynamic_body(url, {
        'server_path': server_path
    })

    # 模拟获得变换矩阵
    transform = locapip.module['fit_fill'].simulate(url, {})
    for t in transform:
        print(t)
```

预期输出

![preview](preview.png)