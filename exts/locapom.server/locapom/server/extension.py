import sys

import carb.settings
import omni.ext

sys.path.append('C:/src/locapi/locapip')

import locapip


class Extension(omni.ext.IExt):
    def on_startup(self, ext_id):
        settings = carb.settings.get_settings()
        port = settings.get_as_int("/exts/locapom.server/port")
        cwd = settings.get_as_string("/exts/locapom.explorer/working_directory")

        locapip.config.update({
            "explorer": {"working_directory": cwd},
            'fit_fill': {},
        })
        locapip.serve(port, False)

    def on_shutdown(self):
        pass
