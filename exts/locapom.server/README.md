# locapom.server

RPC server from locapip

## 更新日志

### 0.1.1

* pip install locapip==0.1.16
* 名称由locapom.rpc.server改为locapom.server
* 增加了explorer模块及其参数

### 0.1.0

* pip install locapip==0.1.14
* 分离了服务作为独立的扩展

## 安装

### Omniverse

安装Omniverse Launcher，在其中安装Create

找到Create的安装路径```path/to/pkg/create-2021.x.x```

也可以在Launcher->Library->Create->Settings找到INSTALL PATH

### locapip

在```path/to/pkg/create-2021.x.x```路径下找到```kit/python```目录

打开终端，为Create内置的Python环境安装locapip

```bat
cmd
python -m pip install -U locapip
```

## 服务端

### 配置项

| 配置 | 类型 | 描述 |
| ------ | ------ | ------ |
| ```exts/locapom.server/port``` | ```int``` | 服务端口 |

### 启动

通过```path/to/pkg/create-2021.x.x```路径下的```omni.create.bat```命令启动服务

```bat
call "path/to/pkg/create-2021.x.x/omni.create.bat" ^
--ext-folder git+https://gitlab.com/locapidio/locapom?branch=master&dir=exts ^
--/exts/locapom.server/port=6547 ^
--enable locapom.server
```
