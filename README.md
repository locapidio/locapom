# locapom

Extensions for [NVIDIA Omniverse](https://developer.nvidia.com/nvidia-omniverse-platform)

### 微服务

* [locapom.server](https://gitlab.com/locapidio/locapom/tree/master/exts/locapom.server)

### 物理模拟

* [locapom.fit_fill](https://gitlab.com/locapidio/locapom/tree/master/exts/locapom.fit_fill)